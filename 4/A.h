#pragma once

class A
{
public:
    A(int num);
    void setNumber(int num);
    int getNumber();
protected:
    int *number;
};