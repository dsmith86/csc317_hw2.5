#include "A.h"
#include "B.h"
#include <iostream>
using namespace std;

int main()
{
    A a1 = A(5);
    cout << "a1: " << a1.getNumber() << endl;
    
    A a2 = a1;
    a2.setNumber(2);
    cout << "a2: " << a2.getNumber() << endl;
    cout << "a1: " << a1.getNumber() << endl;
    
    B b1 = B(5);
    cout << "b1: " << b1.getNumber() << endl;
    
    B b2 = b1;
    b2.setNumber(2);
    cout << "b2: " << b2.getNumber() << endl;
    cout << "b1: " << b1.getNumber() << endl;
    
    return 0;
}