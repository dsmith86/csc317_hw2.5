#include "B.h"

B::B(int num)
{
    number = new int;
    *number = num;
}

B::B(const B& b)
{
    number = new int;
    *number = *b.number;
}

B B::operator=(const B& b)
{
    if (this != &b)
    {
        number = new int;
        *number = *(b.number);
    }
    return *this;
}

B::~B()
{
    delete number;
}

void B::setNumber(int num)
{
    *number = num;
}

int B::getNumber()
{
    return *number;
}