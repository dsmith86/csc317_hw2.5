#pragma once

class B
{
public:
    B(int num);
    B(const B& b);
    B operator=(const B& b);
    ~B();
    void setNumber(int num);
    int getNumber();
protected:
    int *number;
};