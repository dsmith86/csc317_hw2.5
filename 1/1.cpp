#include <iostream>
#include "Computer.h"
#include "InventoryRecord.h"

int main() {
    Computer computer = Computer("Toshiba", "TSH84245");
    
    cout << "Computer:" << endl << "-------" << endl;
    computer.writeFields();
    cout << endl;
    
    InventoryRecord inventoryRecord = InventoryRecord("Dell", "DL4242165", "TEC202", 14124);
    
    cout << "Inventory record:" << endl << "-------" << endl;
    inventoryRecord.writeFields();
    
    return 0;
}
