#include "InventoryRecord.h"

InventoryRecord::InventoryRecord(string b, string s, string l, int i)
    : Computer(b, s)
{
    location = l;
    inventoryNumber = i;
}

string InventoryRecord::getLocation()
{
    return location;
}

int InventoryRecord::getInventoryNumber()
{
    return inventoryNumber;
}

void InventoryRecord::writeFields() const
{
    Computer::writeFields();
    cout << location << endl;
    cout << inventoryNumber << endl;
}