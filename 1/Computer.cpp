#include "Computer.h"

Computer::Computer(string b, string s)
{
    brand = b;
    serialnum = s;
}

string Computer::getBrand() const
{
    return brand;
}

string Computer::getSerialNumber() const
{
    return serialnum;
}

void Computer::writeFields() const
{
    cout << brand << endl;
    cout << serialnum << endl;
}