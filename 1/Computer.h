#pragma once

#include <string>
#include <iostream>
using namespace std;

class Computer
{
public:
    Computer(string b, string s);
    string getBrand() const;
    string getSerialNumber() const;
    void writeFields() const;
private:
    string brand;
    string serialnum;
};