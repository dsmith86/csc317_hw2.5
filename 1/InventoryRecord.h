#include "Computer.h"

#pragma once

class InventoryRecord : public Computer
{
public:
    InventoryRecord(string b, string s, string l, int i);
    string getLocation();
    int getInventoryNumber();
    void writeFields() const;
private:
    string location;
    int inventoryNumber;
};