#include <iostream>
#include "A.h"
#include "B.h"

using namespace std;

int main()
{
    A a = A();
    B b = B();
    A c = b;
    
    a.print();
    b.print();
    b.print("Galaxy");
    c.print();
    
    return 0;
}