//
//  B.cpp
//  HW2.5
//
//  Created by Daniel Smith on 2/23/15.
//
//

#include "B.h"

void B::print()
{
    cout << "Hello, Solary System!" << endl;
}

void B::print(string s)
{
    cout << "Hello, " << s << "!" << endl;
}