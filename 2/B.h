#include "A.h"
#include <iostream>
using namespace std;

#pragma mark

class B : public A
{
public:
    void print();
    void print(string s);
};